export class Movie {
  id: number;
  title: string;
  category: string;
  releaseYear: number;
  poster: string;
  directors: string;
  actors: any;
  synopsis: string;
  rate: number;
  lastViewDate: string;
  price: any
}

