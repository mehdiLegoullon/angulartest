import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainContentComponent} from "./main-content.component";


const routes: Routes = [
  {
    path: '',
    component: MainContentComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('../home/home.module').then(m => m.HomeModule),
      },

      {
        path: 'actors',
        loadChildren: () => import('../actors/actors.module').then(m => m.ActorsModule),
      },

      {
        path: 'movies',
        loadChildren: () => import('../movies/movies.module').then(m => m.MoviesModule),
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainContentRoutingModule {
}
