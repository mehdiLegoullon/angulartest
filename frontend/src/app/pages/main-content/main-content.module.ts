import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainContentComponent} from './main-content.component';
import {RouterModule} from "@angular/router";
import {MainContentRoutingModule} from "./main-content.routing.module";
import {HeaderModule} from "../../components/header/header.module";


@NgModule({
  declarations: [MainContentComponent],
  imports: [
    CommonModule,
    RouterModule,
    MainContentRoutingModule,
    HeaderModule,
  ]
})
export class MainContentModule {
}
