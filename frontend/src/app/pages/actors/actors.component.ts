import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActorsService} from "../../services/actors.service";
import {ActorModel} from "../../models/actor.model";
import {takeUntil, tap} from "rxjs/operators";
import {Subject} from "rxjs";


@Component({
  selector: 'app-actors',
  templateUrl: './actors.component.html',
  styleUrls: ['./actors.component.scss']
})
export class ActorsComponent implements OnInit, OnDestroy {
  actors: ActorModel[];
  destroy$: Subject<boolean> = new Subject();

  constructor(
    private actorsService: ActorsService,
  ) {
  }

  ngOnInit() {
    this.getActors();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  deleteActor(id) {
    return this.actorsService.deleteActorFromDatabase(id)
      .pipe(
        tap(this.getActors),
      )
      .subscribe()
  }

  getActors() {
    this.actorsService.getAllActorsFromDatabase()
      .pipe(
        takeUntil(this.destroy$),
        tap(actor => this.actors = actor),
      ).subscribe()
  }

  createActor() {
    alert('Not working, currently being implemented');
  }
}
