import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActorsComponent} from './actors.component';
import {ActorsRoutingModule} from "./actors.routing.module";
import {MatChipsModule} from "@angular/material/chips";
import {MatIconModule} from "@angular/material/icon";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [ActorsComponent],
  imports: [
    CommonModule,
    ActorsRoutingModule,
    MatChipsModule,
    MatIconModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
  ]
})
export class ActorsModule {
}
