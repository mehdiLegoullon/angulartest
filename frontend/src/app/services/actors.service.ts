import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ActorModel} from "../models/actor.model";

@Injectable({
  providedIn: 'root'
})
export class ActorsService {

  constructor(
    private http: HttpClient,
  ) {
  }

  getAllActorsFromDatabase(): Observable<ActorModel[]> {
    return this.http.get<ActorModel[]>('http://localhost:3015/api/actors');
  }

  deleteActorFromDatabase(id): Observable<ActorModel[]> {
    return this.http.delete<ActorModel[]>('http://localhost:3015/api/actors/' + id);
  }

/*  createActor(): Observable<ActorModel[]> {
    return this.http.post<ActorModel[]>('http://localhost:3015/api/actors');
  }*/


}
